const slugify = require('slugify');
const fs = require('fs');

function changeNameToCamelCase(str) {
  const stringArray = str.split('-');

  stringArray.forEach((part, index) => {
    stringArray[index] = upperCaseFirst(part);
  });

  return `${stringArray.join('')}`;
}

function upperCaseFirst(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

function renameFolder(oldPath, newPath, successString, errorString) {
  fs.rename(oldPath, newPath, function (err) {
    if (err) console.log(errorString);
    console.log(successString);
  });
}

// command line arguments order
const oldNameIndex = process.argv.indexOf('--oldname');
const newNameIndex = process.argv.indexOf('--newname');

//Setup names
let oldName = process.argv[oldNameIndex + 1];
let newName = process.argv[newNameIndex + 1];
let oldNameSlug = slugify(oldName, {
  remove: /[!?#=*+~.()'":@]/g,
}).toLowerCase();
let newNameSlug = slugify(newName, {
  remove: /[!?#=*+~.()'":@]/g,
}).toLowerCase();
let oldNameCamelCase = changeNameToCamelCase(oldNameSlug);
let newNameCamelCase = changeNameToCamelCase(newNameSlug);

//======================================AUDIO===========================================
//rename audio
let oldAudioPath = './src/audio/' + oldNameSlug;
let newAudioPath = './src/audio/' + newNameSlug;
let audioSucces = 'Successfully renamed the directory in audio folder.';
let audioError =
  'Folder in audio was not renamed. Reason: bad old name or there is not yet audio folder for this cviceni.';
renameFolder(oldAudioPath, newAudioPath, audioSucces, audioError);

//======================================DATA===========================================
//rename data file and the assets in it

let oldDataPath = './src/data/' + 'cviceni' + oldNameCamelCase + '.json';
let newDataPath = './src/data/' + 'cviceni' + newNameCamelCase + '.json';
let dataSucces = 'Successfully renamed the directory in data folder.';
let dataError =
  'File in data folder was not renamed. Reason: bad old name or there is different slug.';
//change name in data and all path by old slug
const data = fs.readFileSync(oldDataPath);
let dataWithchanges = JSON.stringify(JSON.parse(data))
  .split(oldNameSlug)
  .join(newNameSlug);
let newData = JSON.parse(dataWithchanges);

newData.cviceni.nazev = newName;
fs.writeFile(oldDataPath, JSON.stringify(newData), 'utf-8', function (err) {
  if (err) throw err;
  console.log('Data in json file was successfully updated.');
  // rename the data file
  renameFolder(oldDataPath, newDataPath, dataSucces, dataError);
});

//======================================IMG===========================================
let oldImgPath = './src/img/' + oldNameSlug;
let newImgPath = './src/img/' + newNameSlug;
let imgSucces = 'Successfully renamed the directory in img folder.';
let imgError =
  'Foleder in img was not renamed. Reason: bad old name or there is not yet img folder for this cviceni.';
renameFolder(oldImgPath, newImgPath, imgSucces, imgError);

//======================================PDF===========================================
let oldPdfPath = './src/pdf/doporuceny-postup/' + oldNameSlug + '.pdf';
let newPdfPath = './src/pdf/doporuceny-postup/' + newNameSlug + '.pdf';
let pdfSucces = 'Successfully renamed the file in pdf folder.';
let pdfError =
  'File in pdf folder was not renamed. Reason: bad old name or there is not yet pdf file for this cviceni.';
renameFolder(oldPdfPath, newPdfPath, pdfSucces, pdfError);

//======================================TEXT===========================================
let oldTextPath = './src/text/' + oldNameSlug;
let newTextPath = './src/text/' + newNameSlug;
let textSucces = 'Successfully renamed the directory in text folder.';
let textError =
  'Foleder in text was not renamed. Reason: bad old name or there is not yet text folder for this cviceni.';
renameFolder(oldTextPath, newTextPath, textSucces, textError);

//======================================VIDEO===========================================
let oldVideoPath = './src/video/' + oldNameSlug;
let newVideoPath = './src/video/' + newNameSlug;
let videoSucces = 'Successfully renamed the directory in video folder.';
let videoError =
  'Foleder in video was not renamed. Reason: bad old name or there is not yet video folder for this cviceni.';
renameFolder(oldVideoPath, newVideoPath, videoSucces, videoError);
//======================================PUG===========================================
let oldPugPath = './src/views/cviceni/' + oldNameSlug;
let newPugPath = './src/views/cviceni/' + newNameSlug;
let pugSucces = 'Successfully renamed the directory in pug folder.';
let pugError =
  'Foleder in pug was not renamed. Reason: bad old name or there is not yet pug folder for this cviceni.';

// change loading pugData in slug (the name of data/json has changed)
const pugData = fs.readFileSync(oldPugPath + '/index.pug', 'utf8');
let pugDataWithchanges = pugData
  .split('cviceni' + oldNameCamelCase)
  .join('cviceni' + newNameCamelCase);

fs.writeFile(oldPugPath + '/index.pug', pugDataWithchanges, 'utf-8', function (
  err
) {
  if (err) throw err;
  console.log('Data in pug file was successfully updated.');
  //rename folder
  renameFolder(oldPugPath, newPugPath, pugSucces, pugError);
});
